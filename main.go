/*
	TODO:
	- Strip punctuation on from input text
	- Make all input lowercase for comparison
	- Branching?
	- Replace hardcoded with a loaded yaml file?
*/

package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"time"
)

type dialogue struct {
	text         string
	keyWords     []string
	nextDialogue *dialogue
}

func (dlg *dialogue) addToEnd(text string, keyWords []string) {
	for dlg.nextDialogue != nil {
		dlg = dlg.nextDialogue
	}
	dlg.nextDialogue = &dialogue{text, keyWords, nil}
}

func printSlowly(text string) {
	fmt.Println()
	for _, char := range text {
		fmt.Printf("%c", char)
		time.Sleep(50 * time.Millisecond)
	}
	fmt.Println()
	fmt.Println()
}

func checkInput(input string, keyWords []string) bool {
	dialoguePassed := false
	wordStart := 0
	wordStarted := false
	inputWords := make([]string, 0)

	// Parse Input
	for i, char := range input {
		if char != ' ' && wordStarted == false {
			wordStart = i
			wordStarted = true
		}
		if (char == ' ' || char == '\n') && wordStarted == true {
			wordStarted = false
			inputWords = append(inputWords, input[wordStart:i])
		}
	}

	// Check input against keywords
	for _, word := range inputWords {
		for _, keyWord := range keyWords {
			if word == keyWord {
				dialoguePassed = true
			}
		}
	}

	return dialoguePassed
}

func checkErr(err error) {
	if err != nil {
		printSlowly("I didn't expect you to be this rude.")
		printSlowly("\n--- Program terminated ---\n")
		log.Fatal(err)
	}
}

func main() {
	reader := bufio.NewReader(os.Stdin)

	// Main dialogue sections
	var dlg *dialogue
	dlg = &dialogue{
		"Good to see you again, son",
		[]string{"Hello", "Doctor"},
		nil,
	}
	dlg.addToEnd(
		"Everything that follows is a result of what you see here.",
		[]string{"Why", "call", "me"},
	)
	dlg.addToEnd(
		"I trust your judgement.",
		[]string{"Normally", "homicide", "detective", "normal"},
	)
	dlg.addToEnd(
		"But then our interactions have never been entirely normal.\nWouldn't you agree?",
		[]string{"Why", "kill", "yourself"},
	)

	// Main loop
	for dlg != nil {
		dialoguePassed := false
		printSlowly(dlg.text)

		for !dialoguePassed {
			input, err := reader.ReadString('\n')
			checkErr(err)
			dialoguePassed = checkInput(input, dlg.keyWords)
			if !dialoguePassed {
				printSlowly("I'm sorry, my responses are limited, you must ask the right questions.")
			}
		}
		dlg = dlg.nextDialogue
	}

	printSlowly("That, Detective, is the right question.")
	printSlowly("--- Program terminated ---\n")
}
